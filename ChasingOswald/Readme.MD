# Chasing Oswald Meta

This meta is an altered copy of Chasing Oswald meta by Joxer, availble here;
http://www.virindi.net/wiki/index.php/Public_Meta_Repository

Files:
ChasingOswald.md
- This readme file
ChasingOswald.utl
- Loot Profile
ChasingOswald.met
- Meta File
ChasingOswald.af
- MetaF formay copy of ChasingOswald.met
- Human readable and hand editable copy of the .met file, not used by vtank directly
- Can be converted to .met using https://github.com/JJEII/metaf 

The following adjustments were made.
- Updated NPC dialog triggers for ACE
- Slight adjusment to portal coordinates
- Slight pathing adjustments in prison to hopefully avoid getting stuck
- Miscallaneous adjustments to item hand-in logic
- Renamed files so that they adhere to a naming standard of my personal prference which groups meta files together when browsing the VTank files directory


Requirements:
- Chracter is level 90+ (140+ reccomended for solo survivability)
- MagTools Decal plugin
- Aphus Lassel Recall
    - The meta is designed to use Aphus Lassel Recall in order to get to town network between tasks.
    - You "can" manually limp the meta through those sections without the recall using the following steps.
        - Pause Vtank when the Meta is trying to Recall to AL (The mouse cursor will be an hourglass even thought he character isn't visually doing anything)
        - Use a Town Network Gem (I reccomend carrying 15 of these)
        - Enterthe summoned Town Network portal.
        - In the VTank Route Tab, click on the following Nav entries to have them removed from the list
            - Recall: Recall Aphus Lassel
            - Pause: 5 Seconds
            - Point: (2.295N, 95.456E)
            - Portal: Portal to Town Network
        - Resume the Macro
        - Vtank should continue along


## Original help text
(This is the original text from ChaseOswaldhelp.txt included with the original meta)


Requires:
Aphus recall (or redo the nav to get to Huntsman of Silyun and Viamontian Prison)*
Mag Tools


Starts as soon as the meta is loaded on default.
Each section is triggered by talking to the Huntsman of Silyun



*If redoing nav, 2 suggestions:
recall to mp then use town network gems.
split viamontion prison nav into 2 sections, first part to eastwatch, then reuse the rest of route.